var controllers = angular.module('starter.controllers', ['ngResource']);

controllers.factory('Base64', function () {
    /* jshint ignore:start */

    var keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

    return {
        encode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            do {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);

                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;

                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }

                output = output +
                    keyStr.charAt(enc1) +
                    keyStr.charAt(enc2) +
                    keyStr.charAt(enc3) +
                    keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);

            return output;
        },

        decode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
            var base64test = /[^A-Za-z0-9\+\/\=]/g;
            if (base64test.exec(input)) {
                window.alert("There were invalid base64 characters in the input text.\n" +
                    "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                    "Expect errors in decoding.");
            }
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

            do {
                enc1 = keyStr.indexOf(input.charAt(i++));
                enc2 = keyStr.indexOf(input.charAt(i++));
                enc3 = keyStr.indexOf(input.charAt(i++));
                enc4 = keyStr.indexOf(input.charAt(i++));

                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;

                output = output + String.fromCharCode(chr1);

                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }

                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";

            } while (i < input.length);

            return output;
        }
    };

    /* jshint ignore:end */
});
controllers.controller('AppCtrl', ["$scope","$ionicModal", "$timeout", "$http",'Base64', function($scope,$ionicModal, $timeout,$http,Base64) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};
  $scope.loginData["j_username"] = "root";
  $scope.loginData["j_password"] = "r0000t";

  $scope.url = "http://10.0.6.236:8181/cms";

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

    function getXMLHttpRequest()
    {
        if (window.XMLHttpRequest) {
            return new window.XMLHttpRequest;
        }
        else {
            try {
                return new ActiveXObject("MSXML2.XMLHTTP.3.0");
            }
            catch(ex) {
                return null;
            }
        }
    }


  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);
    var authdata = Base64.encode($scope.loginData.j_username + ':' + $scope.loginData.j_password);
    $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; // jshint ignore:line
    $http({
        method: 'GET',
        url: "http://10.0.6.236:8181/cms/workflow/workflow/inbox"
    }).success(function successCallback(response) {
        $scope.loggedIn = true;
        $scope.closeLogin();
      })
      .error( function errorCallback(response) {
        alert('Failed to login.');
        //$scope.closeLogin();
    });

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    //$timeout(function() {
    //$scope.closeLogin();
    //}, 1000);
  };

  // Logout, refresh page and (maybe unrelated) hide menu items that are no
  // longer usable.
  $scope.logout = function() {
    $timeout(function() {
    }, 1000);
    $scope.loggedIn = false;
  };

  $scope.$watch(function () {
      // return $ionicSideMenuDelegate.getOpenRatio();
    }, function (value) {
      $scope.loggedIn;
  });
}]);

controllers.controller('InboxCtrl', function($scope, $ionicModal, $timeout) {
  $scope.url = "http://10.0.6.236:8181/cms/workflow/workflow/inbox";
  $scope.inboxData = [];

  $scope.getInbox = function(size, page) {
    $http({
        method: 'GET',
        url: "http://10.0.6.236:8181/cms/workflow/workflow/inbox"
    }).success(function successCallback(response) {
        $scope.inboxData = response.data;
      })
      .error( function errorCallback(response) {
        alert('Failed to login.');
    });
  };
  $scope.getInbox(10, 1);

});
